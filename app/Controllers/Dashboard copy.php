<?php

namespace App\Controllers;

use App\Models\DashboardModel;
use App\Models\PDashboardModel;
use CodeIgniter\Validation\Validation;

class Dashboard extends BaseController
{

    protected $models;
    protected $pmodels;
    public function __construct()
    {
        $this->models = new DashboardModel();
        $this->pmodels = new PDashboardModel();
    }

    public function index()
    {
        $kabupaten = $this->models->findAll();
        $data = [
            'title' => 'Data Provinsi',
            'kabupaten' => $kabupaten

        ];



        return view('dashboard/index', $data);
    }

    public function create()
    {

        session();
        $data = [
            'title' => 'Data Provinsi',
            'validation' => \Config\Services::validation()


        ];



        return view('dashboard/createprov', $data);
    }

    public function save()
    {
        if (!$this->validate([
            'provinsi_name' => 'required|is_unique[provinsi.prov_name]'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('dashboard/create')->withInput()->with('validation', $validation);
        }
        $this->pmodels->save([

            'prov_name' => $this->request->getVar('provinsi_name')
        ]);
        session()->setFlashdata('pesan', 'Provinsi Berhasil Ditambahkan');
        return redirect()->to('/dashboard');
    }

    public function delete($kab_id)
    {
        $this->models->delete($kab_id);
        session()->setFlashdata('pesan', 'Provinsi Berhasil Dihapus');
        return redirect()->to('dashboard/');
    }
    public function edit($kab_id)
    {

        session();
        $data = [
            'title' => 'Data Provinsi',
            'validation' => \Config\Services::validation(),
            'kab' => $this->models->getkabupaten($kab_id)


        ];



        return view('dashboard/editprov', $data);
    }

    public function update($kab_id)
    {
        if (!$this->validate([
            'provinsi_name' => 'required|is_unique[provinsi.prov_name]'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('dashboard/edit/' . $this->request->getVar('kab_id'))->withInput()->with('validation', $validation);
        }
        $this->pmodels->save([
            'kab_id' => $kab_id,
            'prov_name' => $this->request->getVar('provinsi_name')
        ]);
        session()->setFlashdata('pesan', 'Provinsi Berhasil Ditambahkan');
        return redirect()->to('/dashboard');
    }
}
