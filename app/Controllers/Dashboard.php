<?php

namespace App\Controllers;

use App\Models\DashboardModel;
use App\Models\DataModel;
use App\Models\PDashboardModel;
use CodeIgniter\Model;
use CodeIgniter\Validation\Validation;

class Dashboard extends BaseController
{

    protected $models;
    protected $pmodels;
    public function __construct()
    {
        $this->models = new DashboardModel();
        $this->pmodels = new PDashboardModel();
    }

    public function index()
    {

        $data = [
            'title' => 'Data Provinsi',
            'kab' => $this->models->kabupaten(),


        ];



        return view('dashboard/index', $data);
    }
    public function provinsi()
    {

        $data = [
            'title' => 'Data Provinsi',
            'prov' => $this->pmodels->getprovinsi()

        ];



        return view('dashboard/provinsi', $data);
    }

    public function create()
    {

        session();
        $data = [
            'title' => 'Data Provinsi',
            'validation' => \Config\Services::validation()


        ];



        return view('dashboard/createprov', $data);
    }
    public function createkab()
    {

        session();
        $data = [
            'title' => 'Data Provinsi',
            'validation' => \Config\Services::validation(),
            'kab' => $this->pmodels->provinsi()


        ];



        return view('dashboard/createkab', $data);
    }

    public function save()
    {
        if (!$this->validate([
            'provinsi_name' => 'required|is_unique[provinsi.prov_name]'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('dashboard/create')->withInput()->with('validation', $validation);
        }
        $this->pmodels->save([

            'prov_name' => $this->request->getVar('provinsi_name')
        ]);
        session()->setFlashdata('pesan', 'Provinsi Berhasil Ditambahkan');
        return redirect()->to('/dashboard');
    }
    public function savekab()
    {
        // if (!$this->validate([
        //     'kab_name' => 'required|is_unique[kabupaten.kab_name]'
        // ])) {
        //     $validation = \Config\Services::validation();
        //     return redirect()->to('dashboard/createkab')->withInput()->with('validation', $validation);
        // }

        $this->models->save([

            'kab_prov_id' =>   $this->request->getvar('provinsi'),
            'kab_name' => $this->request->getVar('kab_name'),
            'kab_jumlah' => $this->request->getVar('kab_jumlah'),
        ]);
        session()->setFlashdata('pesan', 'Data Berhasil Ditambahkan');
        return redirect()->to('/dashboard');
    }

    public function delete($kab_id)
    {
        $this->models->delete($kab_id);
        session()->setFlashdata('pesan', 'Kabupaten Berhasil Dihapus');
        return redirect()->to('dashboard/');
    }
    public function deleteprov($prov_id)
    {
        $this->pmodels->delete($prov_id);
        session()->setFlashdata('pesan', 'provinsi Berhasil Dihapus');
        return redirect()->to('/dashboard/provinsi');
    }
    public function edit($kab_id)
    {

        session();
        $data = [
            'title' => 'Data Provinsi',
            'validation' => \Config\Services::validation(),
            'kab' => $this->models->getkabupaten($kab_id),
            'prov' => $this->pmodels->provinsi()




        ];



        return view('dashboard/editkab', $data);
    }

    public function editprov($prov_id)
    {

        session();
        $data = [
            'title' => 'Data Provinsi',
            'validation' => \Config\Services::validation(),
            'prov' => $this->pmodels->getprovinsi($prov_id)


        ];



        return view('dashboard/editprov', $data);
    }

    public function updatekab($kab_id)
    {
        // if (!$this->validate([
        //     'provinsi_name' => 'required|is_unique[provinsi.prov_name]'
        // ])) {
        //     $validation = \Config\Services::validation();
        //     return redirect()->to('dashboard/edit/' . $this->request->getVar('kab_id'))->withInput()->with('validation', $validation);
        // }
        $this->models->save([
            'kab_id' => $kab_id,
            'kab_prov_id' =>   $this->request->getvar('provinsi'),
            'kab_name' => $this->request->getVar('kab_name'),
            'kab_jumlah' => $this->request->getVar('kab_jumlah'),
        ]);
        session()->setFlashdata('pesan', 'kabupaten Berhasil Diubah');
        return redirect()->to('/dashboard');
    }
    public function updateprov($prov_id)
    {
        // if (!$this->validate([
        //     'provinsi_name' => 'required|is_unique[provinsi.prov_name]'
        // ])) {
        //     $validation = \Config\Services::validation();
        //     return redirect()->to('dashboard/edit/' . $this->request->getVar('kab_id'))->withInput()->with('validation', $validation);
        // }
        $this->pmodels->save([
            'prov_id' => $prov_id,
            'prov_name' => $this->request->getVar('prov_name'),
        ]);
        session()->setFlashdata('pesan', 'data Berhasil Diubah');
        return redirect()->to('/dashboard/provinsi');
    }
    // public function cari()
    // {
    //     $data = [
    //         'title' => 'Data Provinsi',
    //         'validation' => \Config\Services::validation(),
    //         'kab' => $this->pmodels->provinsi(),

    //     ];
    //     $kabb = $this->models->kabupaten();
    //     $users = new class() extends Model
    //     {
    //         protected $table = 'kabupaten';
    //         public function kabupaten()
    //         {
    //             return $this->db->table('kabupaten')
    //                 ->join('provinsi', 'provinsi.prov_id=kabupaten.kab_prov_id')
    //                 ->get()->getResultArray();
    //         }
    //     };
    //     $cari = $this->request->getVar('cari');
    //     $data = $kabb->where('prov_name', $cari)->findAll();
    //     return view('/dashboard/pencarian', compact('data'));
    // }

    public function filter()
    {

        $key = $this->request->getVar('filter');
        $data = [
            'title' => 'Data Provinsi',
            'kab' => $this->models->filter($key),
            'prov' => $this->pmodels->provinsi()
        ];



        return view('dashboard/index', $data);
    }
    public function pencarian()
    {

        $key = $this->request->getVar('cari');
        $data = [
            'title' => 'Data Provinsi',
            'kab' => $this->models->pencarian($key),

        ];



        return view('dashboard/index', $data);
    }
}
