<?php

namespace App\Models;

use CodeIgniter\Model;

class DashboardModel extends Model
{
    protected $table      = 'kabupaten';
    protected $primaryKey = 'kab_id';
    protected $allowedFields = ['prov_name', 'kab_name', 'kab_prov_id', 'kab_jumlah'];
    public function getkabupaten($kab_id = false)
    {
        if ($kab_id == false) {

            return $this->findAll();
        }
        return $this->where(['kab_id' => $kab_id])->first();
    }
    public function kabupaten()
    {
        return $this->db->table('kabupaten')
            ->join('provinsi', 'provinsi.prov_id=kabupaten.kab_prov_id')
            ->get()->getResultArray();
    }

    public function filter($key)
    {
        return $this->db->table('kabupaten')
            ->join('provinsi', 'provinsi.prov_id=kabupaten.kab_prov_id')->where('prov_name', $key)
            ->get()->getResultArray();
    }

    public function pencarian($key)
    {
        return $this->db->table('kabupaten')
            ->join('provinsi', 'provinsi.prov_id=kabupaten.kab_prov_id')->like('prov_name', $key)->orLike('kab_name', $key)
            ->get()->getResultArray();
    }
}
