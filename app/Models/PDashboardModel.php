<?php

namespace App\Models;

use CodeIgniter\Model;

class PDashboardModel extends Model
{
    protected $table      = 'provinsi';
    protected $primaryKey = 'prov_id';
    protected $allowedFields = ['prov_name', 'prov_id'];

    public function provinsi()
    {

        return $this->db->table('provinsi')->get()->getResultArray();
    }
    public function getprovinsi($prov_id = false)
    {
        if ($prov_id == false) {

            return $this->findAll();
        }
        return $this->where(['prov_id' => $prov_id])->first();
    }
}
