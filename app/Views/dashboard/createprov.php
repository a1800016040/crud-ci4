<!DOCTYPE html>
<html lang="en">

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <div class="container">
        <div class="row">

            <div class="col">
                <h1>Tambah Provinsi</h1>
                <?= $validation->listErrors(); ?>
                <form action="/Dashboard/save" method="POST">
                    <?= csrf_field() ?>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="provinsi_name" name="provinsi_name" placeholder="Masukkan Nama Provinsi">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary my-3">Tambah Provinsi</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

</body>

</html>