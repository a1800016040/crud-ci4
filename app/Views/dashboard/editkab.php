<!DOCTYPE html>
<html lang="en">

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <div class="container">
        <div class="row">

            <div class="col">
                <h1>Ubah Provinsi</h1>
                <?= $validation->listErrors(); ?>
                <form action="/Dashboard/updatekab/<?= $kab['kab_id']; ?>" method="POST">
                    <?= csrf_field() ?>
                    <div class="col-sm-10">

                        <select class="form-select mb-2" name="provinsi" id="provinsi" required>

                            <option value="<?= $kab['kab_prov_id']; ?>" selected> --Ubah Provinsi--</option>

                            <?php
                            foreach ($prov as $item) :
                            ?>
                                <option value="<?= $item['prov_id'] ?>"> <?= $item['prov_name'] ?> </option>
                            <?php
                            endforeach
                            ?>
                        </select>


                        <input type="text" class="form-control" id="kab_name" name="kab_name" value="<?= $kab['kab_name']; ?>" placeholder=" Masukkan Nama kabupaten">

                        <input type="text" class="form-control" id="kab_jumlah" name="kab_jumlah" value="<?= $kab['kab_jumlah']; ?>" placeholder=" Masukkan jumlah penduduk">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary my-3">Ubah Provinsi</button>
                        </div>
                    </div>
                </form>


            </div>
        </div>
    </div>

</body>

</html>