<!DOCTYPE html>
<html lang="en">

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>


    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <br>
                <h3>Pencarian</h3>
                <form action="<?php echo base_url('Dashboard/pencarian') ?>" method="post">
                    <input type="text" name="cari" id="cari" placeholder="masuukan keyword">
                    <button type="submit" class="btn btn-primary" value="cari">cari</button>

                </form>
                <hr>
                <form action="<?php echo base_url('Dashboard/filter') ?>" action="post">
                    <div class="mb-3">
                        <label for="filter">filter provinsi</label>
                        <select name="filter" id="filter">
                            <?php foreach ($kab as $k) : ?>
                                <option value="<?= $k['prov_name']; ?>"><?= $k['prov_name']; ?></option>
                            <?php endforeach; ?>

                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary" value="filter">filter</button>
                </form>
                <a href="/">reset</a>
            </div>
            <div class="row">
                <div class="col">
                    <h1>Data Penduduk</h1>
                    <a href="/Dashboard/create" class="btn btn-primary">Tambah Provinsi</a>
                    <a href="/Dashboard/createkab" class="btn btn-primary">Tambah Kabupaten</a>
                    <a href="/Dashboard/provinsi/" class="btn btn-primary">Edit Provinsi</a>
                    <?php if (session()->getFlashdata('pesan')) : ?>
                        <div class="alert alert-success" role='alert'>

                            <?= session()->getFlashdata('pesan'); ?>

                        </div>
                    <?php endif ?>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">prov id</th>
                                <th scope="col">Provinsi</th>
                                <th scope="col">Kabupaten</th>
                                <th scope="col">Jumlah</th>
                                <th scope="col">kabupaten option</th>


                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1 ?>
                            <?php foreach ($kab as $k) : ?>
                                <tr>
                                    <th scope="row"><?= $i++; ?></th>
                                    <td><?= $k['kab_prov_id']; ?></td>
                                    <td><?= $k['prov_name']; ?></td>
                                    <td><?= $k['kab_name']; ?></td>
                                    <td><?= $k['kab_jumlah']; ?></td>
                                    <td>
                                        <a href="/dashboard/edit/<?= $k['kab_id']; ?>" class="btn btn-primary">edit</a>
                                        <form action="/dashboard/<?= $k['kab_id']; ?>" method="post">

                                            <?= csrf_field() ?>
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class="btn btn-danger" class="d-inline" onclick="return confirm('apakah anda yakin menghapus ?');"> Delete</button>
                                        </form>

                                    </td>
                                </tr>
                            <?php endforeach; ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

</body>

</html>