<!DOCTYPE html>
<html lang="en">

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">prov id</th>
                <th scope="col">Provinsi name</th>


            </tr>
        </thead>
        <tbody>
            <?php $i = 1 ?>
            <?php foreach ($prov as $k) : ?>
                <tr>
                    <th scope="row"><?= $i++; ?></th>
                    <td><?= $k['prov_id']; ?></td>
                    <td><?= $k['prov_name']; ?></td>

                    <td>
                        <a href="/dashboard/editprov/<?= $k['prov_id']; ?>" class="btn btn-primary">edit</a>
                        <a href="/dashboard/deleteprov/<?= $k['prov_id']; ?>" class="btn btn-danger">delete</a>


                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>
</body>

</html>