/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.4.13-MariaDB : Database - jmctest
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`jmctest` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `jmctest`;

/*Data for the table `kabupaten` */

insert  into `kabupaten`(`kab_id`,`kab_prov_id`,`kab_name`,`kab_jumlah`) values 
(13,NULL,'cibinong',5),
(14,NULL,'cibinong',5),
(15,NULL,'jakarta',5),
(16,NULL,'jakarta',5),
(17,NULL,'cibinong',5),
(18,NULL,'cibinong',5),
(19,NULL,'cibinong',10),
(20,NULL,'cibinong',5),
(21,NULL,'banten',500),
(23,NULL,'cibinong',500),
(25,NULL,'banten',10),
(27,NULL,'jakarta',75),
(28,27,'cibinong',500),
(29,29,'jakarta',75),
(30,30,'yunin',10),
(31,30,'banten',10);

/*Data for the table `provinsi` */

insert  into `provinsi`(`prov_id`,`prov_name`) values 
(27,'yunands'),
(28,'sewu kuto'),
(29,'yunan'),
(30,'luyuukss'),
(31,'aikmel');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
